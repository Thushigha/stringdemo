package edu.home.str;

public class booleanendswith {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String x = new String("The Solar System formed around 4.6 billion years ago");
		boolean returnVal;

		returnVal = x.endsWith("years ago");
		System.out.println("Returned Value = " + returnVal);

		returnVal = x.endsWith("ag");
		System.out.println("Returned Value = " + returnVal);
	}

}
