package edu.home.str;

public class intcompareToIgnoreCasestringstring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1 = "Solar System is the gravitationally bound system of the Sun";
		String s2 = "Solar System is the gravitationally bound system of the Sun";
		String s3 = "Solar system consists star and sun";

		int S = s1.compareToIgnoreCase(s2);
		System.out.println(S);

		S = s2.compareToIgnoreCase(s3);
		System.out.println(S);

		S = s3.compareToIgnoreCase(s1);
		System.out.println(S);
	}

}
