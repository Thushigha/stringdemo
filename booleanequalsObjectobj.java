package edu.home.str;

public class booleanequalsObjectobj {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String S1 = new String("The Solar System formed around 4.6 billion years ago");
		String S2 = S1;
		String S3 = new String("The Solar System formed around 4.6 billion years ago");
		boolean retVal;

		retVal = S1.equals(S2);
		System.out.println("Returned Value = " + retVal);

		retVal = S1.equals(S3);
		System.out.println("Returned Value = " + retVal);
	}

}
